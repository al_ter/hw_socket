import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";
import * as textsServices from '../services/textsServices';

const router = Router();

router
  .get("/", (req, res) => {
    const page = path.join(HTML_FILES_PATH, "game.html");
    res.sendFile(page);
  })
  .get("/texts/:id", (req, res) => {
    try {
      const text = textsServices.findById(req.params.id);
      res.json({ id: req.params.id, text });
    } catch (error) {
      res.status(404);
      res.json({ error: error.message });
    }
  });

export default router;
