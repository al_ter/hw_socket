import { texts } from '../data';

export const getOne = id => {
  id = parseInt(id);
  if (id > texts.length - 1 || id < 0 || !Number.isInteger(id)) {
    throw new Error('Wrong id');
  }
  return texts[id];
};
