import * as config from "./config";
import { texts } from '../data';

const Users = new Map(); // user, isReady
const Rooms = new Map();
let theGameIsOn = false;
let Players = [];

export default io => {
  io.on("connection", socket => {
    const { username } = socket.handshake.query;
    const isUserExist = Users.has(username);

    if (isUserExist) {
      console.log(`User ${username} exists`);
      socket.emit('DENY_USER_EXIST');
      socket.disconnect();
    }

    if (Users.size === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
      console.log(`User ${username} denied. Maximum users riched`);
      socket.emit('DENY_MAXIMUM_USERS');
      socket.disconnect();
    }
    
    console.log(`${username} connected`);

    Users.set(username, false);
    io.emit('UPDATE_USERS', [...Users.entries()]);

    socket.on('USER_READY', () => {
      console.log(`${username} ready`);

      Users.set(username, true);
      
      emitUpdateUsers();
      checkGameStart();
    });

    socket.on('USER_NOT_READY', () => {
      console.log(`${username} not ready`);

      Users.set(username, false);
    });

    socket.on('USER_PROGRESS', ({ progress, sec }) => {
      socket.broadcast.emit('UPDATE_PROGRESS', { id: username, progress, sec });
      setPlayer(Players, username, { progress, sec });

      checkGameOn();
    });

    socket.on('disconnect', () => {
      console.log(`${username} disconnected`)

      Users.delete(username);
      emitUpdateUsers();

      if (theGameIsOn) {
        deletePlayer(username);
        checkGameOn();
      } else {
        checkGameStart();
      }
    });

    socket.on('TIME_IS_UP', () => {
      emitGameEnd();
    });

    socket.on('INIT', () => {
      emitUpdateUsers();
    });

    const checkGameStart = () => {
      if (isEverybodyReady(Users)) {
        setGameState(true);
        setPlayers();
        io.emit('GAME_START', {
          countdown: config.SECONDS_TIMER_BEFORE_START_GAME,
          time: config.SECONDS_FOR_GAME,
          textId: pickRandomTextIndex(texts)
        });
      }
      return;
    };

    const checkGameOn = () => {
      if (isEverybodyFinished(Players)) {
        emitGameEnd();
      }
    };

    const emitGameEnd = () => {
      io.emit('GAME_END', Players);
      resetUsers();
      emitUpdateUsers();
    };

    const emitUpdateUsers = () => {
      io.emit('UPDATE_USERS', [...Users.entries()]);
    };
    
  });
};

const isEverybodyReady = map => {
  let everybodyReady = true;
  for (let i of map.values()) {
    everybodyReady = Boolean(i & everybodyReady);
  }

  return everybodyReady;
};

const isEverybodyFinished = players => {
  const totalProgress = players.reduce((sum, player) => {
    return sum + player.progress;
  }, 0);
  return totalProgress === players.length * 100;
};

const getRandomMinMax = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min);
};

const pickRandomTextIndex = array => getRandomMinMax(0, array.length - 1);

const setGameState = state => theGameIsOn = state;

const setPlayers = () => {
  Users.forEach((value, key) => Players.push({
  id: key,
  progress: 0,
  sec: 0
}))};

const deletePlayer = id => {
  Players = Players.filter(i => i.id !== id);
};

const setPlayer = (arr, id, data) => {
  const idx = arr.findIndex(player => player.id === id);
  arr[idx] = { ...arr[idx], ...data };
};

const resetUsers = () => {
  Users.forEach(value => value = false);
}