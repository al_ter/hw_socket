import { createElement, createElementNS, addClass, removeClass, setWidthPercent } from './helper.mjs';
import { socket } from './game.mjs';

const username = sessionStorage.getItem("username");
const gamePage = document.getElementById('game-page');
const roomsPage = document.getElementById('rooms-page');

export const renderGamePage = () => {
  const leftBlock = createLeftBlock();
  const rightBlock = createRightBlock();
  addClass(gamePage, 'full-screen grid-container display-none');
  gamePage.append(leftBlock, rightBlock);
  removeClass(gamePage, 'display-none');
};

export const createLeftBlock = () => {
  const leftBlockContainer = createElement({
    tagName: 'div',
    className: 'left-block'
  });

  const header = createHeader();

  const players = createElement({
    tagName: 'div',
    className: 'users'
  });

  leftBlockContainer.append(header, players);

  return leftBlockContainer;
};

export const createRightBlock = () => {
  const rightBlockContainer = createElement({
    tagName: 'div',
    className: 'right-block flex-centered'
  });

  const buttonReady = createElement({
    tagName: 'button',
    className: 'very big button waiting',
    attributes: { id: 'ready'}
  });

  buttonReady.innerText = 'Press when ready';
  buttonReady.addEventListener('click', onReady);

  const startCountdown = createElement({
    tagName: 'div',
    className: 'display-none',
    attributes: { id: 'start-countdown'}
  });

  const gameText = createElement({
    tagName: 'div',
    className: 'display-none',
    attributes: { id: 'game-text'}
  });

  const done = createElement({
    tagName: 'span',
    className: '',
    attributes: { id: 'text-done'}
  });

  const remained = createElement({
    tagName: 'span',
    className: '',
    attributes: { id: 'text-remain'}
  });

  gameText.append(done, remained);

  const endCountdownContainer = createElement({
    tagName: 'div',
    className: 'countdown-container display-none'
  });

  const endCountdown = createElement({
    tagName: 'span',
    className: '',
    attributes: { id: 'end-countdown'}
  });

  const endCountdownText = document.createTextNode('seconds left: ');

  endCountdownContainer.append(endCountdownText, endCountdown);
  rightBlockContainer.append(buttonReady, startCountdown, gameText, endCountdownContainer);

  return rightBlockContainer;
};

export const createUserContainer = (userId, userStatus) => {
  const userContainer = createElement({
    tagName: 'div',
    className: 'flex user',
    attributes: { id: `user-${userId}`}
  });

  const h3 = createElement({
    tagName: 'h3',
    className: 'flex'
  });

  const svg = createElementNS({
    NS: 'http://www.w3.org/2000/svg',
    tagName: 'svg',
    className: 'user__status'
  });

  userStatus && addClass(svg, 'ready');

  const _circle = createElementNS({
    NS: svg.namespaceURI,
    tagName: 'circle',
    className: '',
    attributes: {
      cx: '10',
      cy: '10',
      r: '10'
    }
  });

  svg.append(_circle);

  const userName = createElement({
    tagName: 'span',
    className: 'user__name'
  });

  const userNameText = document.createTextNode(userId);
  userName.append(userNameText);

  const userYou = createElement({
    tagName: 'span',
    className: 'you material-icons',
    attributes: { title: 'You' }
  });

  userYou.innerText = 'star';

  h3.append(svg, userName);
  username === userId && h3.append(userYou);

  const progressContainer = createElement({
    tagName: 'div',
    className: 'progress-container'
  });

  const progressBar = createElement({
    tagName: 'div',
    className: 'progress-bar'
  });

  progressContainer.append(progressBar);
  userContainer.append(h3, progressContainer);

  return userContainer;
};

export const updateProgress = (id, progress, sec) => {
  const progressBar = document.querySelector(`.user#user-${id} .progress-bar`);
  setWidthPercent(progressBar, progress);

  if (progress === 100) {
    const userCard = document.getElementById(`user-${id}`);
    const userHeader = document.querySelector(`.user#user-${id} h3`);
    const result = createResult(sec);
    userHeader.append(result);
    addClass(userCard, 'finished');
  }
};

export const setUsers = users => {
  const usersBlock = document.querySelector('.users');
  usersBlock.innerHTML = '';
  usersBlock.append(...users);
}

export const createHeader = (roomId = 'Room') => {
  const headerContainer = createElement({
    tagName: 'div',
    className: 'header flex'
  });

  const h1 = createElement({
    tagName: 'h1',
    className: ''
  });

  h1.innerText = roomId;

  const buttonBack = createElement({
    tagName: 'button',
    className: 'big button flex-centered',
    attributes: { id: 'go-back' }
  });

  buttonBack.addEventListener('click', onDisconnect);

  const arrowBack = createElement({
    tagName: 'span',
    className: 'material-icons'
  });

  arrowBack.innerText = 'keyboard_arrow_left';
  const buttonBackText = document.createTextNode('Disconnect');
  buttonBack.append(arrowBack, buttonBackText);
  headerContainer.append(h1, buttonBack);

  return headerContainer;
};

export const createResult = result => {
  const el = createElement({
    tagName: 'span',
    className: 'result'
  });

  el.innerText = `${result} s`;
  return el;
}

/* Handlers */
const isReady = () => sessionStorage.getItem("ready");

const toggleReady = () => {
  const ready = isReady();
  sessionStorage.setItem("ready", !ready);
};

export const onReady = e => {
  const readyMarker = document.querySelector(`#user-${username} .user__status`);
  const buttonReady = e.target;
  toggleReady();
  const ready = isReady();
  
  if (ready) {
    addClass(readyMarker, 'ready');
    removeClass(e.target, 'waiting');
    buttonReady.innerText = 'Waiting for others...';

    socket.emit('USER_READY');
  } else {
    removeClass(readyMarker, 'ready');
    addClass(e.target, 'waiting');
    buttonReady.innerText = 'Press when ready';

    socket.emit('USER_NOT_READY');
  }
};

export const onDisconnect = () => {
  socket.close();
  sessionStorage.clear();
  window.location.replace("/login");
};
