import { createElement, addClass, removeClass, setWidthPercent, fetchText } from './helper.mjs';
import { renderGamePage, createResult, createUserContainer, setUsers, updateProgress } from './render.mjs';

// const gamePage = document.getElementById('game-page');
// const roomsPage = document.getElementById('rooms-page');
// const buttonCreateRoom = document.getElementById('create-room');
// const roomsContainer = document.getElementById('rooms');

const username = sessionStorage.getItem("username");
sessionStorage.setItem("ready", false);

if (!username) {
  window.location.replace("/login");
}

export const socket = io("", { query: { username } });

const startCountdown = countdown => {
  const buttonReady = document.getElementById('ready');
  const buttonGoBack = document.getElementById('go-back');
  const coundownStart = document.getElementById('start-countdown');
  
  addClass(buttonReady, 'display-none');
  addClass(buttonGoBack, 'display-none');
  removeClass(coundownStart, 'display-none');

  return new Promise(resolve => {
    let countdownTtimerId = setInterval(() => {
      coundownStart.innerText = countdown;

      if (countdown === 0) {
        clearInterval(countdownTtimerId);
        addClass(coundownStart, 'display-none');
        resolve();
      }

      countdown--;
    }, 1000); // 1s
  })
};

const startGame = (time, text) => {
  const userCard = document.getElementById(`user-${username}`);
  const gameText = document.getElementById('game-text');
  const gameTextDone = document.getElementById('text-done');
  const gameTextRemain = document.getElementById('text-remain');
  const coundownEndContainer = document.querySelector('.countdown-container');
  const coundownEnd = document.getElementById('end-countdown');
  const userBar = document.querySelector(`.user#user-${username} .progress-bar`);
  let timeLeft = time;

  let currentIndex = 0;
  let nextChar;
  let progress = 0;
  let textDone = '';
  let textRemain = text;

  const onKeyPress = e => {
    e.keyCode === 'Space' && e.preventDefault();
    nextChar = text[currentIndex];

    if (e.key !== nextChar) {
      return;
    }
    
    currentIndex++;
    progress = Math.min((currentIndex / text.length) * 100, 100);
    textDone = text.slice(0, currentIndex);
    textRemain = text.slice(currentIndex);

    gameTextDone.innerText = textDone;
    gameTextRemain.innerText = textRemain;
    setWidthPercent(userBar, progress);
    socket.emit('USER_PROGRESS', { progress, sec: time - timeLeft });

    if (progress === 100) {
      addClass(userCard, 'finished');
      addClass(gameText, 'finished');
      userCard.querySelector('h3').append(createResult(time - timeLeft));
    }
  };

  removeClass(coundownEndContainer, 'display-none');
  document.addEventListener('keydown', onKeyPress, false);

  return new Promise(resolve => {
    let gameTimerId = setInterval(() => {
      coundownEnd.innerText = timeLeft;

      if (timeLeft === 0) {
        clearInterval(gameTimerId);
        addClass(coundownEndContainer, 'display-none');
        document.removeEventListener('keydown', onKeyPress, false);
        resolve();
      }

      timeLeft--;
    }, 1000) // 1s
  })

};

/* Socket events */
socket.on('DENY_USER_EXIST', () => {
  alert(`Username ${username} already exists.`);
  sessionStorage.removeItem("username");
  window.location.replace("/login");
});

socket.on('DENY_MAXIMUM_USERS', () => {
  alert(`Maximum users riched. Try later`);
  sessionStorage.removeItem("username");
  window.location.replace("/login");
});

socket.on('UPDATE_USERS', users => {
  const allUsers = users.map(([id, status]) => createUserContainer(id, status));
  setUsers(allUsers);
});

socket.on('UPDATE_PROGRESS', ({ id, progress, sec }) => {
  updateProgress(id, progress, sec);
});

socket.on('GAME_START', async ({ countdown, time, textId }) => {
  const gameText = document.getElementById('game-text');
  const gameTextRemain = document.getElementById('text-remain');

  const { text } = await fetchText(textId);
  gameTextRemain.innerText = text;

  await startCountdown(countdown);
  removeClass(gameText, 'display-none');
  await startGame(time, text);

  socket.emit('TIME_IS_UP');
});

socket.on('GAME_END', players => {
  debugger
  players.sort((a, b) => a.sec - b.sec);
  const list = players
    .map((p, i) => `${i + 1}. ${p.id}: ${p.sec} s`)
    .join('\n');
    
  alert(list);

  init();
});

const init = () => {
  const buttonReady = document.getElementById('ready');
  const buttonGoBack = document.getElementById('go-back');
  const gameText = document.getElementById('game-text');
  
  removeClass(buttonReady, 'display-none');
  removeClass(buttonGoBack, 'display-none');
  addClass(gameText, 'display-none');
}

renderGamePage();
