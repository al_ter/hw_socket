const textsEndpoint = '/game/texts';

export const createElement = ({ tagName, className, attributes = {} }) => {
  const element = document.createElement(tagName);

  if (className) {
    addClass(element, className);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
};

export const createElementNS = ({ NS, tagName, className, attributes = {} }) => {
  const element = document.createElementNS(NS, tagName);

  if (className) {
    addClass(element, className);
  }

  Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

  return element;
};

export const addClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.add(...classNames);
};

export const removeClass = (element, className) => {
  const classNames = formatClassNames(className);
  element.classList.remove(...classNames);
};

export const formatClassNames = className => className.split(" ").filter(Boolean);

export const setWidthPercent = (element, width) => {
  element.style.width = `${width}%`;
};

export const fetchText = async id => {
  const response = await fetch(`${textsEndpoint}/${id}`);
  const text = await response.json();
  return text;
};
