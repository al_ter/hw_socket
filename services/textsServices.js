import { getOne } from '../repositories/textsRepository';

export const findById = id => {
  try {
    const text = getOne(id);
    return text;
  } catch (error) {
    throw error;
  }
};
